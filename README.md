# 7-dias-de-codigo
### Desafios propostos pela [Alura](https://7daysofcode.io/)
### Participe também [7 DAYS OF CODE](https://7daysofcode.io/)

## Front-End
### [Lógica com Javascript | 7 desafios independentes para quem está começando na programação](https://7daysofcode.io/matricula/logica-programacao)
### [React | Criando uma página de e-commerce com diferentes componentes](https://7daysofcode.io/matricula/react)
### [HTML e CSS | Criando uma página completa de 'Vagas' de uma empresa fictícia](https://7daysofcode.io/matricula/html-css)
### [HTML e CSS | Criando uma réplica da página de perfil de usuário do LinkedIn](https://7daysofcode.io/matricula/html-css-linkedin)
### [HTML e CSS | Criando uma réplica da página principal de filmes da Netflix](https://7daysofcode.io/matricula/html-css-netflix)
### [JavaScript e DOM | Criando uma calendário de aniversários com CRUD básico](https://7daysofcode.io/matricula/javascript-e-dom)
### [JavaScript e DOM | Consumindo a API de filmes do 'The Movie DataBase' e criando uma página HTML de exibição](https://7daysofcode.io/matricula/javascript-e-dom-api)
### [Responsividade | Criando uma página responsiva para uma clínica médica, a fim de ter controle sobre todas as consultas agendadas](https://7daysofcode.io/matricula/responsividade)

## Back-End 
### [Java | Consumindo a API de filmes do IMDB e criando uma página HTML de exibição](https://7daysofcode.io/matricula/java)
### [Windows Forms | Construindo uma aplicação Desktop e consumindo uma API Rest](https://7daysofcode.io/matricula/windows-forms)
### [C# | Consumindo a API do Pokémon para criar uma versão moderna do Tamagotchi, o bichinho virtual](https://7daysofcode.io/matricula/csharp)
### [Kotlin | Consumindo a API de filmes do IMDB e criando uma aplicação Desktop](https://7daysofcode.io/matricula/kotlin)
### [Spring | Consumindo a API de filmes do IMDB e criando uma página HTML de exibição](https://7daysofcode.io/matricula/spring)
### [Python | Desafios em breve!](https://7daysofcode.io/matricula/python)

## Data Science
### [Ciência de dados | Analisando o dataset do CEAPS, que contém as cotas parlamentares dos Senadores do Brasil](https://7daysofcode.io/matricula/data-science)

## Mobile
### [Android | Criando uma aplicação que consome a API de usuários e repositórios do GitHub](https://7daysofcode.io/matricula/android)

## Outros
### [GitHub | Pratique seus conhecimentos de GitHub criando e publicando um projeto](https://7daysofcode.io/matricula/github)
### [SQL | Desafios em breve!]()
### [Flutter | Desafios em breve!]()
### [MongoDB | Desafios em breve!]()
